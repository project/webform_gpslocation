<?php
/**
 * @file
 * Location component for Webform.
 */

/**
 * Define the location component.
 *
 * Implements hook_webform_component_info().
 */
function webform_gpslocation_webform_component_info() {
  $components = [];
  $components['gpslocation'] = [
    'label' => 'Location (GPS)',
    'description' => '',
    'features' => [
      'analysis' => FALSE,
      'csv' => FALSE,
      'default_value' => TRUE,
      'description' => TRUE,
      'email' => FALSE,
      'email_address' => FALSE,
      'email_name' => FALSE,
      'required' => TRUE,
      'title' => TRUE,
      'title_display' => TRUE,
      'title_inline' => TRUE,
      'conditional' => FALSE,
      'group' => FALSE,
      'spam_analysis' => FALSE,
      'attachment' => FALSE,
      'views_range' => FALSE,
    ],
    'file' => 'components/gpslocation_component.inc',
  ];

  return $components;
}

/**
 * Implements hook_element_info().
 */
function webform_gpslocation_element_info() {
  $types = array();

  // Note: Only useable as webform component.
  $types['webform_gpslocation_gpslocation'] = [
    '#input' => TRUE,
  ];

  return $types;
}

/**
 * Test whether the point $lat, $lon is within the specified polygon.
 *
 * This function doesn't work for polygons across the poles. It also does not
 * use compensation for earth's curvature. When large edges are needed and
 * accuracy is degraded, shorten edges by adding seemingly spurious vertices.
 *
 * See http://alienryderflex.com/polygon/
 *
 * @param string $lat
 *   Latitude of the point.
 * @param string $lon
 *   Longitude of the point.
 * @param string $polygon
 *   A string of lat,lon pairs describing the vertices separated by ;.
 *
 * @return bool
 *   TRUE when the point is within the polygon, FALSE otherwise.
 */
function webform_gps_location_is_in_polygon($lat, $lon, $polygon) {
  $x = $lon;
  $y = $lat;

  // Separate the polygon into an array of points.
  $vertices = [];
  $pairs = explode(';', trim($polygon, "\n\r;"));
  foreach ($pairs as $pair) {
    list($lat, $lon) = explode(',', $pair);
    $vertices[] = [
      'lat' => trim($lat),
      'lon' => trim($lon),
    ];
  }

  $is_inside = FALSE;
  $last_vertex = end($vertices);

  bcscale(10);

  foreach ($vertices as $vertex) {
    $x1 = $last_vertex['lon'];
    $x2 = $vertex['lon'];
    $dx = bcsub($x2, $x1);

    if (bccomp(ltrim($dx, '-'), '180') == 1) {
      // Normalize to support polygons across dateline.
      if (bccomp($x, '0') == 1) {
        while (bccomp($x1, '0') == -1) {
          $x1 = bcadd($x1, '360');
        }
        while (bccomp($x2, '0') == -1) {
          $x2 = bcadd($x2, '360');
        }
      }
      else {
        while (bccomp($x1, '0') == 1) {
          $x1 = bcsub($x1, '360');
        }
        while (bccomp($x2, '0') == 1) {
          $x2 = bcsub($x2, '360');
        }
      }
      $dx = bcsub($x2, $x1);
    }

    $x1x = bccomp($x1, $x);
    $x2x = bccomp($x2, $x);
    if (
      // x1 <= x && x2 > x.
    (($x1x == -1 || $x1x == 0) && $x2x == 1)
    ||
    // x1 >= x && x2 < x.
    (($x1x == 1 || $x1x == 0) && $x2x == -1)) {
      $grad = bcdiv(bcsub($vertex['lat'], $last_vertex['lat']), $dx);
      $intersect = bcadd($last_vertex['lat'], bcmul(bcsub($x, $x1), $grad));

      if (bccomp($intersect, $y) == 1) {
        $is_inside = !$is_inside;
      }
    }
    $last_vertex = $vertex;
  }
  return $is_inside;
}
